# translation of kio_tags.po to Francais
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Mickael Sibelle <kimael@gmail.com>, 2008.
# xavier <xavier.besnard@neuf.fr>, 2012, 2013.
# Vincent Pinon <vpinon@kde.org>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: kio_tags\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-16 02:14+0000\n"
"PO-Revision-Date: 2017-12-08 17:02+0100\n"
"Last-Translator: Vincent Pinon <vpinon@kde.org>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 2.0\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: kio_tags.cpp:110 kio_tags.cpp:166
#, kde-format
msgid "File %1 already has tag %2"
msgstr "Le fichier « %1 » possède déjà la balise « %2 »"

#: kio_tags.cpp:298
#, fuzzy, kde-format
#| msgid "Tag"
msgctxt "This is a noun"
msgid "Tag"
msgstr "Balise"

#: kio_tags.cpp:304
#, fuzzy, kde-format
#| msgid "Tag Fragment"
msgctxt "This is a noun"
msgid "Tag Fragment"
msgstr "Fragment de balise"

#: kio_tags.cpp:316 kio_tags.cpp:317
#, kde-format
msgid "All Tags"
msgstr "Toutes les balises"

#~ msgid "Tags"
#~ msgstr "Étiquettes"
